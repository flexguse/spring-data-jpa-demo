package de.flexguse.demo.auditing;

import org.springframework.data.domain.AuditorAware;

import de.flexguse.demo.model.User;

/**
 * This is just a demonstration implementation of {@link AuditorAware}.
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class DemoAuditorAware implements AuditorAware<User> {

	private User currentAuditor = null;

	/**
	 * @param currentAuditor
	 *            the currentAuditor to set
	 */
	public void setCurrentAuditor(User currentAuditor) {
		this.currentAuditor = currentAuditor;
	}

	@Override
	public User getCurrentAuditor() {
		return currentAuditor;
	}

}
