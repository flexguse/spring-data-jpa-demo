/**
 * 
 */
package de.flexguse.demo.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

/**
 * A {@link User} is someone who interacts with the system. This may be a
 * library employee who creates {@link Book}s and {@link Author}s in the system
 * or the library member who borrows books.
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
@Entity
@Table(name = "USER")
public class User extends CustomAbstractAuditable {

	private static final long serialVersionUID = -2358622858820003380L;

	private String firstName;

	private String lastName;

	private String userNumber;

	@ManyToMany(fetch = FetchType.LAZY)
	private List<Book> borrowedBooks = new ArrayList<>();

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the userNumber
	 */
	public String getUserNumber() {
		return userNumber;
	}

	/**
	 * @param userNumber
	 *            the userNumber to set
	 */
	public void setUserNumber(String userNumber) {
		this.userNumber = userNumber;
	}

	/**
	 * @return the borrowedBooks
	 */
	public List<Book> getBorrowedBooks() {
		return borrowedBooks;
	}

	/**
	 * @param borrowedBooks
	 *            the borrowedBooks to set
	 */
	public void setBorrowedBooks(List<Book> borrowedBooks) {
		this.borrowedBooks = borrowedBooks;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((borrowedBooks == null) ? 0 : borrowedBooks.hashCode());
		result = prime * result
				+ ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result
				+ ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result
				+ ((userNumber == null) ? 0 : userNumber.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (borrowedBooks == null) {
			if (other.borrowedBooks != null)
				return false;
		} else if (!borrowedBooks.equals(other.borrowedBooks))
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (userNumber == null) {
			if (other.userNumber != null)
				return false;
		} else if (!userNumber.equals(other.userNumber))
			return false;
		return true;
	}

}
