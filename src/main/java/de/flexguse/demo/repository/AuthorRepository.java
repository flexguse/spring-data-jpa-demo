/**
 * 
 */
package de.flexguse.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import de.flexguse.demo.model.Author;

/**
 * @author Christoph Guse, info@flexguse.de
 *
 */
@Repository
public interface AuthorRepository extends JpaRepository<Author, Long> {

}
