/**
 * 
 */
package de.flexguse.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import de.flexguse.demo.model.Book;

/**
 * @author Christoph Guse, info@flexguse.de
 *
 */
@Repository
public interface BookRepository extends JpaRepository<Book, Long> {

}
