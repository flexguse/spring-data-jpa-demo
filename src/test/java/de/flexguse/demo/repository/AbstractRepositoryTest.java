package de.flexguse.demo.repository;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import de.flexguse.demo.model.Author;
import de.flexguse.demo.model.User;

/**
 * Abstract test class containing the spring configuration used for the tests.
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/repository-context.xml")
public class AbstractRepositoryTest {

	/**
	 * This helper method creates a filled Author object.
	 * 
	 * @return
	 */
	protected Author createAuthor() {
		Author author = new Author();
		author.setFirstName("Simon");
		author.setLastName("Becket");
		author.seteMail("simon.becket@book.de");
		return author;
	}

	/**
	 * This helper method creates a filled User object.
	 * 
	 * @return
	 */
	protected User createAuditingUser() {

		User auditor = new User();
		auditor.setFirstName("Hans");
		auditor.setLastName("Meiser");
		auditor.setUserNumber("H1");

		return auditor;

	}

}
