/**
 * 
 */
package de.flexguse.demo.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import de.flexguse.demo.auditing.DemoAuditorAware;
import de.flexguse.demo.model.Author;
import de.flexguse.demo.model.User;

/**
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class AuthorRepositoryTest extends AbstractRepositoryTest {

	@Autowired
	UserRepository userRepository;

	@Autowired
	private AuthorRepository authorRepository;

	@Autowired
	DemoAuditorAware demoAuditorAware;

	@Rollback(true)
	@Transactional
	@Test
	public void testSaveAuthor() {

		prepareAuditingUser();

		Author author = createAuthor();

		// check no authors are persisted
		List<Author> existentAuthors = authorRepository.findAll();
		assertTrue(existentAuthors.isEmpty());

		authorRepository.save(author);
		existentAuthors = authorRepository.findAll();

		assertEquals("Simon Becket expected to be persisted", 1,
				existentAuthors.size());

		// check if auditing was done
		Author check = existentAuthors.get(0);
		assertNotNull("createdBy expected", check.getCreatedBy());
		assertNotNull("created date expected", check.getCreatedDate());
		assertEquals(author.geteMail(), check.geteMail());
	}

	/**
	 * Create the auditing user. This is done in this helper method because the
	 * user creation should be done in the DB Transaktion of the testmethod so
	 * the user is rolled back after the test.
	 */
	private void prepareAuditingUser() {

		User auditor = createAuditingUser();

		userRepository.save(auditor);
		demoAuditorAware.setCurrentAuditor(auditor);

	}

}
