/**
 * 
 */
package de.flexguse.demo.repository;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import de.flexguse.demo.model.Author;
import de.flexguse.demo.model.Book;

/**
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class BookRepositoryTest extends AbstractRepositoryTest {

	@Autowired
	private BookRepository bookRepository;

	@Autowired
	private AuthorRepository authorRepository;

	/**
	 * This test checks if a book having and author is correctly persisted. The
	 * auditing is not done and tested.
	 */
	@Rollback(true)
	@Transactional
	@Test
	public void testSaveBook() {

		Book book = new Book();

		book.setIsbnNumber("1234567890");
		book.setDescription("This is a wonderfil book.");
		book.setTitle("The Art of living");

		// the author must be existend in the persistence when it is assigned to
		// the book -> CascadingType
		Author author = createAuthor();
		authorRepository.save(author);
		book.setAuthor(author);
		
		bookRepository.save(book);
		
		// check a book was persisted
		List<Book> availableBooks = bookRepository.findAll();
		assertEquals(1, availableBooks.size());
		
		// check if the author is available
		Book check = availableBooks.get(0);
		assertEquals(author, check.getAuthor());
	}

}
