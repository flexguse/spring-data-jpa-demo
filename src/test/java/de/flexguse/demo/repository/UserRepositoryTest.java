/**
 * 
 */
package de.flexguse.demo.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import de.flexguse.demo.auditing.DemoAuditorAware;
import de.flexguse.demo.model.Author;
import de.flexguse.demo.model.Book;
import de.flexguse.demo.model.User;

/**
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class UserRepositoryTest extends AbstractRepositoryTest {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private DemoAuditorAware demoAuditorAware;

	@Autowired
	private AuthorRepository authorRepository;

	@Autowired
	private BookRepository bookRepository;

	@Rollback(true)
	@Transactional
	@Test
	public void testSaveUser() {

		User auditor = new User();
		auditor.setFirstName("The");
		auditor.setLastName("Auditor");
		auditor.setUserNumber("007");
		userRepository.save(auditor);
		demoAuditorAware.setCurrentAuditor(auditor);

		User user = new User();
		user.setFirstName("Test");
		user.setLastName("User");
		user.setUserNumber("123456");
		userRepository.save(user);

		List<User> allUsers = userRepository.findAll();
		assertEquals(2, allUsers.size());
		assertTrue(allUsers.contains(auditor));
		assertTrue(allUsers.contains(user));
		assertEquals(auditor, allUsers.get(1).getCreatedBy());

	}

	@Rollback(true)
	@Transactional
	@Test
	public void testSaveUserWithBooks() {

		// create the author of the books in the persistence
		Author author = createAuthor();
		authorRepository.save(author);

		/*
		 * create 2 books. the books must be persisted before they are assigned
		 * to a user.
		 */
		// create the first book
		Book book1 = new Book();
		book1.setAuthor(author);
		book1.setDescription("This is the first book");
		book1.setIsbnNumber("1");
		book1.setTitle("Book 1");
		bookRepository.save(book1);
		
		Book book2 = new Book();
		book2.setAuthor(author);
		book2.setDescription("This is the second book");
		book2.setIsbnNumber("2");
		book2.setTitle("Book 2");
		bookRepository.save(book2);
		
		/*
		 * create and persist a user which has both books as borrowed books
		 */
		User user = createAuditingUser();
		user.setBorrowedBooks(Arrays.asList(book1, book2));
		userRepository.save(user);
		
		/*
		 * check the persisted user
		 */
		List<User> existentUsers = userRepository.findAll();
		assertEquals("user expected", 1, existentUsers.size());
		
		// check the borrowed books
		List<Book> borrowedBooks = user.getBorrowedBooks();
		assertEquals(2, borrowedBooks.size());
		assertTrue(borrowedBooks.contains(book1));
		assertTrue(borrowedBooks.contains(book2));

	}

}
